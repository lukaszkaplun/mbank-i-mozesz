$(document).ready(function() {
  var mockProgress = null;
  var mockProgressTime = 4000;

  var mqls = [
    window.matchMedia("(max-width: 991px) and (orientation: landscape)"),
    window.matchMedia("(min-width: 991px)")
  ];
  if (!$(".page-wrapper").hasClass("mock")) {
    var mbankSlider = new Swiper(".slides-wrapper", {
      mousewheel: true,
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
      },
      pagination: {
        el: ".swiper-pagination",
        type: "bullets",
        clickable: true
      },
      on: {
        slideChangeTransitionEnd: function() {
          handleVideo(this.activeIndex);
        },
        slideChangeTransitionStart: function() {
          stopMockProgress();
        }
      },
      breakpoints: {
        1200: {
          direction: "vertical"
        }
      }
    });
  }

  function handleIsMobile() {
    if (
      /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino|android|ipad|playbook|silk/i.test(
        navigator.userAgent || navigator.vendor || window.opera
      ) ||
      /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
        (navigator.userAgent || navigator.vendor || window.opera).substr(0, 4)
      )
    ) {
      return true;
    } else {
      return false;
    }
  }
  function startMockProgress() {
    mockProgress = setTimeout(function() {
      mbankSlider.slideNext();
    }, mockProgressTime);
  }
  function stopMockProgress() {
    $(".progress-bar").removeClass("mock-progress");
    clearTimeout(mockProgress);
  }
  function handleVideo(activeIndex) {
    var activeSectionName = $(
      "section[data-section='" + activeIndex + "']"
    ).attr("data-section-name");

    switch (activeSectionName) {
      case "start":
        $(".cta-wrapper").hide();
        $(".cta-wrapper .promo-button").addClass("hidden");
        break;
      case "disclaimer":
        $(".cta-wrapper .promo-button").addClass("hidden");
        $(".footer").removeClass("inverted");
        $(".footer").removeClass("no-links");
        $(".logo .subclaim").removeClass("inverted");
        $(".age-selector-header").removeClass("inverted");
        $(".cta-wrapper").hide();
        $(".footer").addClass("inverted");
        $(".logo .subclaim").addClass("inverted");
        $(".age-selector-header").addClass("inverted");
        break;
      case "benefits":
        $(".cta-wrapper .promo-button").addClass("hidden");
        $(".footer").addClass("no-links");
        $(".logo .subclaim").addClass("inverted");
        $(".age-selector-header").addClass("inverted");
        break;
      case "piatka":
        $(".footer").removeClass("inverted");
        $(".footer").removeClass("no-links");
        $(".logo .subclaim").removeClass("inverted");
        $(".age-selector-header").removeClass("inverted");
        $(".cta-wrapper").show();
        $(".cta-wrapper .promo-button").removeClass("hidden");
        break;
      default:
        $(".cta-wrapper").show();
        $(".cta-wrapper .promo-button").addClass("hidden");
        $(".footer").removeClass("inverted");
        $(".footer").removeClass("no-links");
        $(".logo .subclaim").removeClass("inverted");
        $(".age-selector-header").removeClass("inverted");
    }

    $.each($(".progress-bar"), function(i, el) {
      var sectionRealIndex = parseInt(
        $(el)
          .closest("section")
          .attr("data-section")
      );

      if (sectionRealIndex === activeIndex) {
        if (sectionRealIndex !== 5) {
          $(el).addClass("mock-progress");
          startMockProgress();
        }
        if (sectionRealIndex === 5) {
          $(el).hide();
        }
      }
    });
  }

  function mediaqueryresponse(mql) {
    if (mqls[0].matches) {
      if (!handleIsMobile()) {
        $(".please-rotate")
          .find("h2")
          .text("Rozszerz okno");
        $(".please-rotate")
          .find(".desktop-icon")
          .show();
        $(".please-rotate")
          .find(".mobile-icon")
          .hide();
      }

      $(".please-rotate").show();
      $(".page-wrapper").hide();
    } else {
      $(".please-rotate").hide();
      $(".page-wrapper").show();
    }
  }

  $.each($('[data-section-name="benefits"] .list-wrapper ul li'), function(
    i,
    el
  ) {
    $(el).css({
      "margin-left": 1 + 1.5 * i + "em"
    });
  });

  for (var i = 0; i < mqls.length; i++) {
    mediaqueryresponse(mqls[i]);
    mqls[i].addListener(mediaqueryresponse);
  }

  $(document).on("click", ".chevron.flip-90", function() {
    $(".legal .copy").animate(
      {
        scrollTop: $(".legal .copy").offset().top + 100
      },
      700
    );
  });

  $(document).on("click", "[data-promo-button]", function() {
    $(".overlay.interactive").show();
    setTimeout(function() {
      $(".overlay.interactive .modal").fadeIn();
    }, 300);
  });

  $(document).on("click", "[data-prevent]", function(e) {
    e.preventDefault();
  });
  $(document).on("click", "[data-close], [data-cover]", function() {
    $(".overlay.interactive .modal").fadeOut();
    setTimeout(function() {
      $(".overlay.interactive").hide();
    }, 300);
  });
});
