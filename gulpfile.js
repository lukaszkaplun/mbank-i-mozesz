var gulp = require("gulp"),
  sass = require("gulp-sass"),
  postcss = require("gulp-postcss"),
  autoprefixer = require("autoprefixer"),
  cssnano = require("cssnano"),
  sourcemaps = require("gulp-sourcemaps"),
  browserSync = require("browser-sync").create();

var paths = {
  styles: {
    src: "/css/*.scss",
    dest: "css"
  }

  // Easily add additional paths
  // ,html: {
  //  src: '...',
  //  dest: '...'
  // }
};

function style(done) {
  gulp
    .src("./assets/css/*.scss")
    .pipe(sourcemaps.init())
    .pipe(sass())
    .on("error", sass.logError)
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest("./assets/css/"))
    // Add browsersync stream pipe after compilation
    .pipe(browserSync.stream());

    done();
   
}
function scripts(done) {
  gulp
  .src("./assets/scripts/*.js")
  .pipe(browserSync.stream());

    done();
   
}





function watch(done) {
 
  browserSync.init({
    server: {
      baseDir: "./"
    }
  });
  gulp.watch("./assets/css/*.scss", style);
  gulp.watch("./assets/scripts/*.js", scripts);

 gulp.watch("./*.html").on('change', browserSync.reload);
 gulp.watch("./js/*.js").on('change', browserSync.reload);
 done();

}

// Don't forget to expose the task!
exports.style = style;
exports.watch = watch;
